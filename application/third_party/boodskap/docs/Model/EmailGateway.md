# EmailGateway

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host** | **string** |  | 
**port** | **int** |  | 
**user** | **string** |  | 
**password** | **string** |  | 
**primary_email** | **string** |  | 
**bounce_email** | **string** |  | [optional] 
**ssl** | **bool** |  | [optional] 
**tls** | **bool** |  | [optional] 
**debug** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


