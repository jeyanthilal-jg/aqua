# Swagger\Client\RetreiveUserPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUserProperty**](RetreiveUserPropertyApi.md#getUserProperty) | **GET** /user/property/get/{atoken}/{ouid}/{name} | Retreive User Property


# **getUserProperty**
> \Swagger\Client\Model\UserProperty getUserProperty($atoken, $ouid, $name)

Retreive User Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RetreiveUserPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$ouid = "ouid_example"; // string | Owner User ID
$name = "name_example"; // string | 

try {
    $result = $apiInstance->getUserProperty($atoken, $ouid, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RetreiveUserPropertyApi->getUserProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **ouid** | **string**| Owner User ID |
 **name** | **string**|  |

### Return type

[**\Swagger\Client\Model\UserProperty**](../Model/UserProperty.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

