# Swagger\Client\DeleteStoredUserGroupPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteUserGroupProperty**](DeleteStoredUserGroupPropertyApi.md#deleteUserGroupProperty) | **DELETE** /user/group/property/delete/{atoken}/{ouid}/{gid}/{name} | Delete Stored UserGroupProperty


# **deleteUserGroupProperty**
> \Swagger\Client\Model\Success deleteUserGroupProperty($atoken, $ouid, $gid, $name)

Delete Stored UserGroupProperty

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeleteStoredUserGroupPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$ouid = "ouid_example"; // string | Owner User ID
$gid = 56; // int | User Group ID
$name = "name_example"; // string | 

try {
    $result = $apiInstance->deleteUserGroupProperty($atoken, $ouid, $gid, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteStoredUserGroupPropertyApi->deleteUserGroupProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **ouid** | **string**| Owner User ID |
 **gid** | **int**| User Group ID |
 **name** | **string**|  |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

