# Swagger\Client\RemoveMembersSFromDomainAssetGroupApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**removeMembersFromDomainAssetGroup**](RemoveMembersSFromDomainAssetGroupApi.md#removeMembersFromDomainAssetGroup) | **POST** /domain/asset/group/remove/{atoken}/{gid} | Remove Members(s) from Domain Asset Group


# **removeMembersFromDomainAssetGroup**
> \Swagger\Client\Model\Success removeMembersFromDomainAssetGroup($atoken, $gid, $asset_ids)

Remove Members(s) from Domain Asset Group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemoveMembersSFromDomainAssetGroupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$gid = 56; // int | Group Id
$asset_ids = array(new \Swagger\Client\Model\string[]()); // string[] | Array of asset IDs

try {
    $result = $apiInstance->removeMembersFromDomainAssetGroup($atoken, $gid, $asset_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemoveMembersSFromDomainAssetGroupApi->removeMembersFromDomainAssetGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **gid** | **int**| Group Id |
 **asset_ids** | **string[]**| Array of asset IDs |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

