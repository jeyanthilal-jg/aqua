# Swagger\Client\DeleteAllStoredDomainUserGroupPropertiesApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAllDomainUserGroupProperties**](DeleteAllStoredDomainUserGroupPropertiesApi.md#deleteAllDomainUserGroupProperties) | **DELETE** /domain/user/group/property/deleteall/{atoken}/{gid} | Delete All Stored Domain User Group Properties


# **deleteAllDomainUserGroupProperties**
> \Swagger\Client\Model\Success deleteAllDomainUserGroupProperties($atoken, $gid)

Delete All Stored Domain User Group Properties

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeleteAllStoredDomainUserGroupPropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in domain
$gid = 56; // int | 

try {
    $result = $apiInstance->deleteAllDomainUserGroupProperties($atoken, $gid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteAllStoredDomainUserGroupPropertiesApi->deleteAllDomainUserGroupProperties: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in domain |
 **gid** | **int**|  |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

