# Swagger\Client\UploadFileApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**uploadFile**](UploadFileApi.md#uploadFile) | **POST** /files/upload/{atoken} | Upload File


# **uploadFile**
> \Swagger\Client\Model\IDResult uploadFile($atoken, $binfile, $ispublic, $id)

Upload File

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UploadFileApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$binfile = "/path/to/file.txt"; // \SplFileObject | File binary file content
$ispublic = true; // bool | True if public access is allowed
$id = "id_example"; // string | if id specified, update operation performed

try {
    $result = $apiInstance->uploadFile($atoken, $binfile, $ispublic, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UploadFileApi->uploadFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **binfile** | **\SplFileObject**| File binary file content |
 **ispublic** | **bool**| True if public access is allowed | [optional]
 **id** | [**string**](../Model/.md)| if id specified, update operation performed | [optional]

### Return type

[**\Swagger\Client\Model\IDResult**](../Model/IDResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

