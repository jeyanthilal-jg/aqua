# Swagger\Client\ListUsersApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listUsers**](ListUsersApi.md#listUsers) | **GET** /user/list/{atoken}/{pageSize} | List Users


# **listUsers**
> \Swagger\Client\Model\User[] listUsers($atoken, $page_size, $direction, $email)

List Users

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ListUsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$page_size = 56; // int | Maximum number of users to be listed
$direction = "direction_example"; // string | If direction is specified, **email** is required
$email = "email_example"; // string | Last or First email of the previous list operation, **required** if   **direction** is specified

try {
    $result = $apiInstance->listUsers($atoken, $page_size, $direction, $email);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ListUsersApi->listUsers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **page_size** | **int**| Maximum number of users to be listed |
 **direction** | **string**| If direction is specified, **email** is required | [optional]
 **email** | **string**| Last or First email of the previous list operation, **required** if   **direction** is specified | [optional]

### Return type

[**\Swagger\Client\Model\User[]**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

