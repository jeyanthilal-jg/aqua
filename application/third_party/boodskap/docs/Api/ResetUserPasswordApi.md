# Swagger\Client\ResetUserPasswordApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**resetUserPassword**](ResetUserPasswordApi.md#resetUserPassword) | **GET** /domain/password/reset/{email} | Register User Password


# **resetUserPassword**
> \Swagger\Client\Model\Success resetUserPassword($email)

Register User Password

Reset a user's password, platform will send an email with temporary password

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResetUserPasswordApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$email = "email_example"; // string | Email ID

try {
    $result = $apiInstance->resetUserPassword($email);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResetUserPasswordApi->resetUserPassword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **string**| Email ID |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

