<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>AquaSystem</title>
<meta name="description" content="AquaSystem Admin">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" href="apple-icon.png">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/themify-icons.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/flag-icon.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/cs-skin-elastic.css">
<!-- <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/bootstrap-select.less"> -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/scss/style.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
</head>
<body class="bg-dark">
<div class="fullscreen-bg">
  <video loop muted="" autoplay poster="img/videoframe.jpg" class="fullscreen-bg__video">
    <source src="<?php echo base_url();?>assets/video/bg.mp4" type="video/mp4">
  </video>
</div>
<div class="sufee-login d-flex align-content-center flex-wrap">
  <div class="container">
    <div class="login-content">
      <div class="login-logo">
        <div class="eye">
          <div class="eye-ball"></div>
        </div>
        <a href="index.html"> <img class="align-content" src="<?php echo base_url();?>assets/images/login-logo.png" alt=""> </a> </div>
          <form id="login-form" class="login-form" action="<?php echo base_url('login/loginRequest');?>" method="post">
            <?php if($this->session->flashdata('invalid_user')) {
                          ?>
            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show"> <?php echo $this->session->flashdata('invalid_user');  ?> </div>
            <?php } ?>
            <div class="form-group">
              <label>Email address</label>
              <input type="email" name="username" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
          </form>
    </div>
  </div>
</div>
</div>
<script src="<?php echo base_url();?>assets/js/vendor/jquery-2.1.4.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/popper.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/plugins.js"></script> 
<script src="<?php echo base_url();?>assets/js/main.js"></script>
</body>
</html>
