{
    "query": {
        "constant_score" : {
            "filter" : {
                 "bool" : {
                    "should" : [
                       { "term" : { "pondname" : "__pondname__" } }
                    ],
					"must_not" : {
						"term" : {"sampling_state" : "true"}
					  }
                }
            }
        }
    },
	"size" : __size__,
	"from": __from__,
	"sort": { "__orderfld__" : {"order" : "__orderdir__"} }
}
