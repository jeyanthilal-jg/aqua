jQuery(document).ready(function($) { 

    $.validator.addMethod("chkduplicate", function(value, element, arg) {
		console.log(value);
		var pms = JSON.parse(arg);
		pms["search"] = value ;
		var rkey = $(element).closest("form").find("input[name=rkey]").val() || "";
		pms["rkey"] = rkey;
		var chk_url = site_url + "common/check_duplicate";
		var flg = false;
		$.ajax({
		    	type:"GET",
		    	data: pms,
		    	url: chk_url,
				async:false,
		}).done(function(resp){
			var rdata = JSON.parse(resp);
			console.log(resp);
			console.log(rdata.status);
			if(rdata.status == "success"){
				console.log("success");
				flg = true;
			} else{
				console.log("fail");
				flg = false;
			}
			
		}).fail(function(err){
			console.log(err);
		});
		console.log("123>>>>>>");
		//console.log(pms);
		return flg;
    }, "This field value already exists.");

	
	$("ul.pagination li a").addClass("page-link");
	
	
	 if( $("select[name=no_items]").length > 0 )
	 {
		 var per_page = $("select[name=no_items]").attr("data-per_page") || "";
		 if(per_page!=""){
			 $("select[name=no_items]").val(per_page);
		 }
	 }
	
	 $(document).on("change", "select[name=no_items]", function(){
		/* var v = $("select[name=per_page]").val();
		$("input[name=no_items]").val(v); */
		$("#srchfrm").submit();
	 });
	
	$("#pondlist-form1").validate({
		rules: {
			pondname: {
				required: true,
				chkduplicate: '{ "table_name": "ponds1", "fld_name": "pondname"}'
			},
			width: {
				required: true,
				number: true,
				min:0
			},
			height: {
				required: true,
				number: true,
				min:0
			},
			depth: {
				required: true,
				number: true,
				min:0
			},
			built_date: {
				required: true,
			},
		},
		messages: {

			pondname: {
				required: disp_text("err_pname"),
				chkduplicate: disp_text("err_pname_exists")
			},
			width: {
				required: disp_text("err_pwidth"),
				number : disp_text("err_vwidth"),
				min : disp_text("err_gt0")
			},
			height: {
				required: disp_text("err_plength"),
				number : disp_text("err_vlength"),
				min : disp_text("err_gt0")
			},
			depth: {
				required: disp_text("err_pdepth"),
				number : disp_text("err_vdepth"),
				min : disp_text("err_gt0")
			},
			built_date: {
				required: disp_text("err_date"),
			}
	},
	errorPlacement: function(error, element) {
		
		if (element.attr("name") == "built_date" )
			error.insertAfter(".datetimepicker-icon");
		else
			error.insertAfter(element);
		}

	});
	
});